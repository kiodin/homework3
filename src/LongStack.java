import jdk.nashorn.internal.ir.IfNode;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.LinkedList;

public class LongStack {

    public static void main(String[] argum) {
        // TODO!!! Your tests here!
    }

    private LinkedList<Long> mag;

    LongStack() {
        // TODO!!! Your constructor here!
        mag = new LinkedList<>();

    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        LongStack clonearray = new LongStack();
        for (int i = 0; i < mag.size(); i++) {
            clonearray.push(mag.get(i));
        }
        return clonearray;

    }

    public boolean stEmpty() {
        // TODO!!! Your code here!
        if (this.mag.size() == 0) {
            return true;

        }
        return false;
    }

    public void push(long a) {
        // TODO!!! Your code here
        this.mag.add(a);

    }

    public long pop() {
        // TODO!!! Your code here!
        return mag.removeLast();
    }

    public void op(String s) {
        // TODO!!!
        if (s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/")) {
            if (mag.size() > 1) {
                long First_part = pop();
                long Second_part = pop();
                switch (s) {
                    case "+":
                        push(Second_part + First_part);
                        break;
                    case "-":
                        push(Second_part - First_part);
                        break;
                    case "*":
                        push(Second_part * First_part);
                        break;
                    case "/":
                        push(Second_part / First_part);
                        break;
                }
            } else {
                throw new RuntimeException("In this message " + mag + "we have less than 2 elemts");
            }
        } else {
            throw new RuntimeException("In this message " + mag + ", we have a wrong character like:" + s);
        }
    }

    public long tos() {
        // TODO!!! Your code here!
        long top = pop();
        push(top);
        //return mag.getLast();
        return top;
    }

    @Override
    public boolean equals(Object o) {
        // TODO!!! Your code here!
        return o.equals(mag);
    }

    @Override
    public String toString() {
        StringBuilder sona = new StringBuilder();
        for (int i = 0; i < mag.size(); i++) {
            sona.append(mag.get(i));
        }
        return sona.toString();


    }

    public static long interpret(String pol) {
        int counter = 0;
        LongStack longStack = new LongStack();
        String[] splittin = pol.split("\\s+");
        String[] filtered = Arrays.stream(splittin)
                .filter(item -> !item.isEmpty())
                .toArray(String[]::new);
        for (String str : filtered) {
            boolean isNumber = true;
            long convertToInteger = 0;
            try {
                convertToInteger = Long.parseLong(str);
            } catch (Exception e) {
                isNumber = false;
            }
            if (isNumber) {
                longStack.push(convertToInteger);
                counter++;
            } else {
                longStack.op(str);
                counter--;
            }

        }

        if (counter != 1) {
            // TODO!!! Change the exception message
            throw new RuntimeException("Put your exception text here");
        }

        return longStack.pop();
    }
}

